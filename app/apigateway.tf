data "aws_lambda_function" "existing_lambda" {
  depends_on    = [aws_lambda_function.this]
  function_name = var.name
}

resource "aws_api_gateway_rest_api" "this" {
  name        = var.name
  description = "Test"
}

resource "aws_api_gateway_resource" "this" {
  rest_api_id = aws_api_gateway_rest_api.this.id
  parent_id   = aws_api_gateway_rest_api.this.root_resource_id
  path_part   = ""
}

resource "aws_api_gateway_method" "this" {
  rest_api_id   = aws_api_gateway_rest_api.this.id
  resource_id   = aws_api_gateway_resource.this.id
  http_method   = "ANY"
  authorization = "NONE"
}

resource "aws_api_gateway_integration" "this" {
  rest_api_id             = aws_api_gateway_rest_api.this.id
  resource_id             = aws_api_gateway_resource.this.id
  http_method             = aws_api_gateway_method.this.http_method
  integration_http_method = "ANY"
  type                    = "AWS_PROXY"
  uri                     = data.aws_lambda_function.existing_lambda.invoke_arn
}

resource "aws_api_gateway_method_response" "this" {
  rest_api_id = aws_api_gateway_rest_api.this.id
  resource_id = aws_api_gateway_resource.this.id
  http_method = aws_api_gateway_method.this.http_method
  status_code = "200"
}

resource "aws_api_gateway_deployment" "this" {
  depends_on = [
    aws_api_gateway_method.this,
    aws_api_gateway_integration.this,
  ]
  rest_api_id = aws_api_gateway_rest_api.this.id
  stage_name  = var.stage
}

resource "aws_api_gateway_api_key" "this" {
  name  = var.api_key_name
  value = var.api_key_value
}

resource "aws_api_gateway_usage_plan" "this" {
  name = "helloworld-localstack-test-plan"
  api_stages {
    api_id = aws_api_gateway_rest_api.this.id
    stage  = aws_api_gateway_deployment.this.stage_name
  }
  product_code = "000000"
}

resource "aws_api_gateway_usage_plan_key" "this" {
  key_id        = aws_api_gateway_api_key.this.id
  key_type      = "API_KEY"
  usage_plan_id = aws_api_gateway_usage_plan.this.id
}


resource "aws_acm_certificate" "this" {
  domain_name       = var.api_domain_name
  validation_method = "DNS"
}

resource "aws_api_gateway_domain_name" "this" {
  domain_name     = var.api_domain_name
  certificate_arn = aws_acm_certificate.this.arn
  security_policy = "TLS_1_2"
  endpoint_configuration {
    types = ["REGIONAL"]
  }
}

resource "aws_api_gateway_base_path_mapping" "this" {
  domain_name = aws_api_gateway_domain_name.this.domain_name
  api_id      = aws_api_gateway_rest_api.this.id
  stage_name  = aws_api_gateway_deployment.this.stage_name
}
